export interface AnimationInterface {

  /**
   * Method run requestAnimationFrame
   */
  loop();

  /**
   * Update item positions
   */
  updatePosition(delta: number);

  /**
   * Render objects on canvas
   */
  renderObjects();

  /**
   * Collision detection
   */
  collisionDetection(obj: Object);
}
