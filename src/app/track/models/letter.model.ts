export class Letter {

  /**
   *
   * @param {string} letterName
   * @param {number} letterSpeed
   * @param {Array<number>} letterPosition
   * @param {number} letterDirection
   * @param {number} letterWidth
   */
  constructor(
    public letterName: string,
    public letterSpeed: number = 0, // Letter speed by default
    public letterPosition: Array<number> = [0, 28], // Initial letter position on SVG
    public letterDirection: number = 1, // 1 - from the left to the right, 0 - from the right to the left
    public letterWidth?: number, // horizontal measure of the letter
  ) {}

}
