import { AfterViewInit, Component, ElementRef, HostListener, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Letter } from './models/letter.model';
import { AnimationInterface } from './animation.interface';

@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.css']
})
export class TrackComponent implements OnInit, AfterViewInit, AnimationInterface {
  @ViewChild('trackSvg') trackSvg: ElementRef;
  trackForm: FormGroup;
  hasSubmitted = false;
  hasRemoved = false;
  private lettersOnTrack = [];
  trackWidth: number;
  errors = [];
  lastRenderTime = 0;

  constructor( private renderer: Renderer2 ) {}

  loop() {
    if (Object.keys(this.lettersOnTrack).length > 0) {
      const delta = performance.now() - this.lastRenderTime;
      this.updatePosition(delta);
      this.renderObjects();
      this.lastRenderTime = performance.now();

      requestAnimationFrame(() => this.loop());
    }
  }

  updatePosition(delta: number) {
    Object.keys(this.lettersOnTrack).forEach( (key) => {
      const letter = this.lettersOnTrack[key] as Letter;
      let x = letter.letterPosition[0];
      const colLetter = this.collisionDetection(letter) as Letter;

      if (colLetter !== undefined) {
        letter.letterPosition[0] = letter.letterDirection === 1 ?
          letter.letterPosition[0] -= delta : letter.letterPosition[0] += delta;
        letter.letterDirection = letter.letterDirection === 1 ?
          letter.letterDirection = 0 : letter.letterDirection = 1;

        colLetter.letterPosition[0] = colLetter.letterDirection === 1 ?
          colLetter.letterPosition[0] -= delta : colLetter.letterPosition[0] += delta;
        colLetter.letterDirection = colLetter.letterDirection === 1 ?
          colLetter.letterDirection = 0 : colLetter.letterDirection = 1;
      }

      if ((x >= this.trackWidth && letter.letterDirection === 1) || (x <= 0 && letter.letterDirection === 0)) {
        letter.letterDirection = letter.letterDirection === 0 ? 1 : 0;
      }

      if (letter.letterDirection === 1) {
        x += letter.letterSpeed;
      } else {
        x -= letter.letterSpeed;
      }

      letter.letterPosition[0] = x;
    });
  }

  renderObjects() {
    Object.keys(this.lettersOnTrack).forEach( (key) => {
      const letter = this.lettersOnTrack[key] as Letter;

      if (letter.letterSpeed !== 0) {
        const txtEl = this.renderer.selectRootElement('#al_' + letter.letterName);
        this.renderer.setAttribute(txtEl, 'x', letter.letterPosition[0].toString());

        const txtElText = this.renderer.createText(letter.letterName);
        this.renderer.appendChild(txtEl, txtElText);
      }
    });
  }

  collisionDetection(letter: Letter): object | undefined {
    let colLetter;

    Object.keys(this.lettersOnTrack).forEach( (key) => {
      const eachLetter = this.lettersOnTrack[key] as Letter;

      if ((letter.letterPosition[0] + letter.letterWidth >= eachLetter.letterPosition[0]) &&
        (letter.letterPosition[0] <= eachLetter.letterPosition[0] + eachLetter.letterWidth) &&
        (eachLetter.letterName !== letter.letterName)) {
        colLetter = eachLetter;
      }
    });

    return colLetter;
  }

  onSubmit() {
    this.hasSubmitted = true;
    const letterName = this.trackForm.get('letterName').value;

    if (this.lettersOnTrack[letterName] === undefined && this.trackForm.get('letterSpeed').value === 0) {
      this.errors['emptyLetterError'] = true;
      return false;
    }

    if (!this.trackForm.valid) {
      return false;
    }

    if (this.lettersOnTrack[letterName] === undefined) {
      const letter = new Letter(
        this.trackForm.get('letterName').value,
        this.trackForm.get('letterSpeed').value
      );

      this.lettersOnTrack[letter.letterName] = letter;
      this.addOnTrack(letter);
    } else {
      const letter = this.lettersOnTrack[letterName] as Letter;
      letter.letterSpeed = this.trackForm.get('letterSpeed').value;
    }

    if (Object.keys(this.lettersOnTrack).length === 1) {
      requestAnimationFrame(() => this.loop());
    }

    this.letterFormReset();
  }

  removeLetter() {
    this.hasRemoved = true;
    const letterName = this.trackForm.get('letterName').value;

    if (letterName === '' || this.lettersOnTrack[letterName] === undefined) {
      this.errors['removeLetterError'] = 'You must enter Letter from the Track.';
      return false;
    }

    if (this.lettersOnTrack[letterName] !== undefined) {
      this.renderer.selectRootElement('#al_' + letterName).remove();
      delete this.lettersOnTrack[letterName];
    }

    this.letterFormReset();
  }

  addOnTrack(letter: Letter) {
    const textEl = this.renderer.createElement('text', 'svg');
    this.renderer.setAttribute(textEl, 'x', letter.letterPosition[0].toString());
    this.renderer.setAttribute(textEl, 'y', letter.letterPosition[1].toString());
    this.renderer.setAttribute(textEl, 'class', 'letterOnTrack');
    this.renderer.setAttribute(textEl, 'id', 'al_' + letter.letterName);

    const textElText = this.renderer.createText(letter.letterName);
    this.renderer.appendChild(textEl, textElText);
    this.renderer.appendChild(this.trackSvg.nativeElement, textEl);

    letter.letterWidth = (textEl as HTMLElement).clientWidth;
  }

  ngOnInit() {
    this.letterFormInit();
  }

  letterFormInit() {
    this.trackForm = new FormGroup({
      letterName: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z]+$')]),
      letterSpeed: new FormControl(0)
    });
  }

  letterFormReset() {
    this.trackForm.reset();
    this.letterFormInit();
    this.hasSubmitted = false;
    this.hasRemoved = false;
  }

  ngAfterViewInit() {
    this.setTrackWidth();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.setTrackWidth();
  }

  setTrackWidth() {
    this.trackWidth = (this.trackSvg.nativeElement as HTMLElement).clientWidth - 35;
  }
}
